 <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.html">
                            <img src="images/icon/logo.png" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                      <li class="has-sub">
                            <a class="js-arrow" href="inicio.html">
                                <i class="fas fa-home"></i>Inicio</a>        
                        </li>
                        <li class="has-sub">
                                <a class="js-arrow" href="#">
                                    <i class="far fa-arrow-alt-circle-up"></i>Producto</a>
                            
                             <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li>
                                        <a href="index.html">Carga Masiva</a>
                                    </li>
                                    <li>
                                        <a href="#">Descarga CSV</a>
                                    </li>
                            </ul>
                        </li>
                        <li>
                            <a href="procesador.html">
                                <i class="fa fa-search"></i>Buscador</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->
