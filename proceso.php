<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Carga Masiva</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
         
       <?php
       include("header.php");
       include("menu.php");
       ?>

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <br>
            <center><a href="carga.php" class="btn btn-danger"> Volver a cargar archivo</a></center>

            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                   <!-- <h2 class="title-1">overview</h2> -->

                                   <!-- <button class="au-btn au-btn-icon au-btn--blue">
                                        <i class="zmdi zmdi-plus"></i>add item</button> -->

                                </div>
                            </div>
                        </div>
                     
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
                                    <div class="au-card-title" style="background-image:url('images/icon/hevs.png');">
                                        <div class="bg-overlay bg-overlay--blue"></div>
                                        <h3>
                                            <i class="fa fa-upload" aria-hidden="true"></i>Archivos Cargados</h3>
                                        
                                    </div>
                                    <div class="au-task js-list-load">
                                        

                                        <div class="au-task__footer">
                                            <?php

//obtenemos el archivo .csv
$tipo = $_FILES['archivo']['type'];

$tamanio = $_FILES['archivo']['size'];

$archivotmp = $_FILES['archivo']['tmp_name'];

//cargamos el archivo
$lineas = file($archivotmp);


//inicializamos variable a 0, esto nos ayudará a indicarle que no lea la primera línea
$i=0;

//Recorremos el bucle para leer línea por línea
foreach ($lineas as $linea_num => $linea)
{ 
   //abrimos bucle
   /*si es diferente a 0 significa que no se encuentra en la primera línea 
   (con los títulos de las columnas) y por lo tanto puede leerla*/
   if($i != 0) 
   { 
       //abrimos condición, solo entrará en la condición a partir de la segunda pasada del bucle.
       /* La funcion explode nos ayuda a delimitar los campos, por lo tanto irá 
       leyendo hasta que encuentre un ; */
       $datos = explode(",",$linea);
   
       //Almacenamos los datos que vamos leyendo en una variable
       //usamos la función utf8_encode para leer correctamente los caracteres especiales
        
        $id = $datos[0];

        $tipo = strlen($datos[1]);
   
        if ($tipo == 6) {
            
            $url2 = "https://31327114a904853136f9e80703d2d463:a30fd5cf16a0dbd685c6c1bea8f4d1cc@hechoenvenezuela2.myshopify.com/admin/products/".$id.".json";
                // consultar un producto simple para obtener el inventori id 

                
                $ch = curl_init($url2);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

                $producto = curl_exec($ch); 
                curl_close ($ch);
                $products = json_decode($producto, true);       
                $inventario_id = $products['product']['variants'][0]['inventory_item_id'];
                $cantidad = $products['product']['variants'][0]['inventory_quantity'];
                $sku = $products['product']['variants'][0]['sku'];
                
                //var_dump($products);
                
                /// fin de inventario id 



                $products_array = array(
                    "product"=>array(
                        'id' => $id,

                        "variants"=>array(
                                        array(
                                        "price"=>$datos[2],
                                        "sku"=>$sku,
                                        )
                        )
                    )
                );
                $nuevo = json_encode($products_array);

                
                
                // cambia el precio inicio y el sku 
            
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url2);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_VERBOSE, 0);
                curl_setopt($curl, CURLOPT_HEADER, 1);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($curl, CURLOPT_POSTFIELDS, $nuevo);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                $response = curl_exec ($curl);
                curl_close ($curl);

                // fin de cambio de preci y sku 

                
                /* obtener el nuevo inventory id */
                $ch = curl_init($url2);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

                $producto = curl_exec($ch); 
                curl_close ($ch);
                $products = json_decode($producto, true);       
                $inventario_id = $products['product']['variants'][0]['inventory_item_id'];





                /* PARA CAMBAIR EL TRACKER */


                    $tracker = array(
                    "inventory_item"=>array(
                        'tracked' => true,

                    
                    )
                );


                $track1 = json_encode($tracker);

                $urltrac = "https://31327114a904853136f9e80703d2d463:a30fd5cf16a0dbd685c6c1bea8f4d1cc@hechoenvenezuela2.myshopify.com/admin/inventory_items/".$inventario_id.".json";


                $trackl = curl_init();
                curl_setopt($trackl, CURLOPT_URL, $urltrac);
                curl_setopt($trackl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($trackl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($trackl, CURLOPT_VERBOSE, 0);
                curl_setopt($trackl, CURLOPT_HEADER, 1);
                curl_setopt($trackl, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($trackl, CURLOPT_POSTFIELDS, $track1);
                curl_setopt($trackl, CURLOPT_SSL_VERIFYPEER, false);
                $tarck3 = curl_exec ($trackl);
                
                curl_close ($trackl);

                /**fin del tracker */
                /// actualizando el producto simple el inventario 
                        $intem = array(
                        "location_id"=> 4154064944,
                        "inventory_item_id"=> $inventario_id,
                        "available"=> $cantidad,
                        );
                        $array1 = json_encode($intem);
                        
                        $url1 = "https://31327114a904853136f9e80703d2d463:a30fd5cf16a0dbd685c6c1bea8f4d1cc@hechoenvenezuela2.myshopify.com/admin/inventory_levels/set.json";


                        $curl3 = curl_init();
                        curl_setopt($curl3, CURLOPT_URL, $url1);
                        curl_setopt($curl3, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                        curl_setopt($curl3, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl3, CURLOPT_VERBOSE, 0);
                        curl_setopt($curl3, CURLOPT_HEADER, 1);
                        curl_setopt($curl3, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($curl3, CURLOPT_POSTFIELDS, $array1);
                        curl_setopt($curl3, CURLOPT_SSL_VERIFYPEER, false);
                        $response3 = curl_exec ($curl3);
                        
                        curl_close ($curl3);

        

                // fin de actualizacion 



        }elseif ($tipo == 8) {
        
            /**VARIACIONES **/
            $products_array2 = array(               
                    
                    "variant"=>array(
                                   
                                    "price"=>$datos[6],
                                    "sku"=>$datos[0],
                                    
                                   
                                   
                    )
                
            );
        $nuevo2 =    json_encode($products_array2);
        

            $url = "https://31327114a904853136f9e80703d2d463:a30fd5cf16a0dbd685c6c1bea8f4d1cc@hechoenvenezuela2.myshopify.com/admin/variants/".$id.".json";
        
            
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_VERBOSE, 0);
            curl_setopt($curl, CURLOPT_HEADER, 1);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($curl, CURLOPT_POSTFIELDS,$nuevo2 );
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec ($curl);
            curl_close ($curl);








        }





        
        echo "<br> listo el producto <a href='https://hechoenvenezuela2.myshopify.com/admin/products/$id'>".$id; echo "</a><br>";







   }

   /*Cuando pase la primera pasada se incrementará nuestro valor y a la siguiente pasada ya 
   entraremos en la condición, de esta manera conseguimos que no lea la primera línea.*/
   $i++;
   //cerramos bucle
}
?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                         

                                   
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
