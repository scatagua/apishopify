 <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="images/icon/logo.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="has-sub">
                            <a class="js-arrow" href="index.php">
                                <i class="fas fa-home"></i>Inicio</a>        
                        </li>
                        <li class="has-sub">
                                <a class="js-arrow" href="#">
                                    <i class="far fa-arrow-alt-circle-up"></i>Producto</a>
                            
                             <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li>
                                        <a href="carga.php">Carga Masiva</a>
                                    </li>
                                    <li>
                                        <a href="#">Descarga CSV</a>
                                    </li>
                            </ul>
                        </li>
                        <li>
                            <a href="buscador.php">
                                <i class="fa fa-search"></i>Buscador</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->