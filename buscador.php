<!DOCTYPE html>
<html lang="en">

<?php
error_reporting(0);

?>

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title></title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition" background="images/canaima.jpg">
    <div class="page-wrapper">
      
        <div class="logo">
                <a href="index.php">
                    <img src="images/icon/logo.png" alt="HEVS" />
                </a>
            </div>
       
    
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4"></div>
                           <div class="col-lg-4">

                                <div class="card">
                                    <div class="card-header"><center>Ingrese el sku del producto</center></div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">SKU</h3>
                                        </div>
                                        <hr>
                                        <form action="#" method="post" >
                                            
                                            <div class="form-group has-success">
                                                
                                                <input id="cc-name" name="id" type="text" class="form-control cc-name valid" data-val="true" data-val-required="Please enter the name on card" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error" autofocus>
                                                <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                                            </div>
                                            
                                    </div>
                                            <div>
                                                <button id="payment-button" type="submit" name="enviar" class="btn btn-lg btn-info btn-block">
                                                    <i class="fa fa-search fa-lg"></i>&nbsp;
                                                    <span id="payment-button-amount">Buscar</span>
                                                    <span id="payment-button-sending" style="display:none;">Buscando…</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            


                                <?php
                                    if (isset($_POST['enviar'])) {
                                            $mysqli = new mysqli("localhost", "root", "", "hev_api");

                                            if ($mysqli->connect_errno) {
                                                echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
                                            }
                                            
                                            $seacrh = $_POST['id'];
                                            $aux = explode("-", $seacrh);
                                           $ver = sizeof($aux);
                                            
                                            $resultado1 = $mysqli->query("SELECT idapi FROM producto WHERE sku ='$seacrh'");

                                            

                                            if ($resultado1->num_rows > 0 ) {
                                            
                                                $row1 = mysqli_fetch_assoc($resultado1);
                                                $id = $row1["idapi"];
                                                if ($ver < 2 ){
                                            

                                                    $ch1 = curl_init("https://31327114a904853136f9e80703d2d463:a30fd5cf16a0dbd685c6c1bea8f4d1cc@hechoenvenezuela2.myshopify.com/admin/products/".$id.".json");
                                                }
                                                else{

                                                    /*
                                                       entra aca cuando es una variacion 
                                                       y lo primero es consultar a la variacion  mediante el id de base de datos      
                                                    */

                                                    $ch1 = curl_init("https://31327114a904853136f9e80703d2d463:a30fd5cf16a0dbd685c6c1bea8f4d1cc@hechoenvenezuela2.myshopify.com/admin/variants/".$id.".json");
                                                  /*  echo "https://31327114a904853136f9e80703d2d463:a30fd5cf16a0dbd685c6c1bea8f4d1cc@hechoenvenezuela2.myshopify.com/admin/variants/".$id.".json";*/
                                                    /*
                                                       aca buscamos el id del producto padre para traer las imagenes     
                                                    */
                                                    $idimh = $mysqli->query("SELECT idapi FROM producto WHERE sku ='".$aux[0]."'");
                                                    $idimh1 = mysqli_fetch_assoc($idimh);
                                                    $idimg = $idimh1["idapi"];
                                                    $imgurl = curl_init("https://31327114a904853136f9e80703d2d463:a30fd5cf16a0dbd685c6c1bea8f4d1cc@hechoenvenezuela2.myshopify.com/admin/products/".$idimg.".json");

                                                    curl_setopt($imgurl, CURLOPT_RETURNTRANSFER, true);
                                                    curl_setopt($imgurl, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

                                                    $imgrespond = curl_exec($imgurl); 



                                                    $imags = json_decode($imgrespond, true);
                                                       /*   echo "<pre>";
                                                print_r($imags );*/

                                                }

                                                curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                                                curl_setopt($ch1, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

                                                $response1 = curl_exec($ch1); 

                                                $producto = json_decode($response1, true);
                                               /*echo "<pre>";
                                                print_r($producto );*/

                            

                                                if ($ver < 2 ){
                                                    //simple 
                                                        $titulo = $producto['product']['title'] ;
                                                        $sku = $producto['product']['variants'][0]['sku'];
                                                        $precio =$producto['product']['variants'][0]['price'];
                                                        $cantidad =$producto['product']['variants'][0]['inventory_quantity'];
                                                        $body_html = $producto['product']['body_html'] ;
                                                        $img = $producto['product']['images'][0]['src'];


                                                                                                              
                                                                        
                                                                            
                                                }else{
                                                        $titulo = $imags['product']['title'] ;
                                                        $body_html = $imags['product']['body_html'] ;
                                                        $sku = $producto['variant']['sku'];
                                                        $precio =$producto['variant']['price'];
                                                        $cantidad =$producto['variant']['inventory_quantity'];
                                                        $img = $imags['product']['images'][0]['src'];

                                                        

                                                        
                                                }

                                            }else{ ?>
                                                
                                                <h1 style="color:#ffffff" text-align="center">Debe sincronizar o este producto no existe</h1>
                                                <br><br>

                                                <?php

                                            }
                                 $resultado1->close();
                                    }
                                ?>
                            </div>

                                <!-- PRODUCTO BUSCADO -->
                        <div class="col-lg-2">

                        </div>
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header"><center>Producto</center></div>
                                    <div class="card-body">
                                        <div class="card-title">
                                           <center><h1>
                                                 <?php 
                                                 echo $titulo ;
                                                 ?>
                                                </h1></center>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-4">
                                            <img src="images/huella.png" class="rounded float-left" alt="...">
                                                
                                            </div>
                                            <div class="col-md-8">
                                                <br><br>
                                                
                                                <h1><center><?php 
                                                 echo $sku ;
                                                 ?></center></h1>
                                                <h2><center>Precio: BsS: <?php echo $precio?></center></h2>
                                                <!-- <h3>Cantidad <?php echo $cantidad?></h3> -->
                                                <br> <p>
                                                <?php 
                                                //echo $body_html;
                                                ?>
                                                </p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>




                            </div> 

                         
                    </div>
                </div>


            </div>


        </div>

    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>

                       